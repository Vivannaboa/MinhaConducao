# Projeto Xamarin

Minha Condução

Nossa proposta tem a finalidade de ajudar as pessoas que precisam de algum tipo de condução por exemplo para ir a faculdade que muitas vezes precisam ficar aguardando o ônibus em um local inseguro sem saber quanto tempo o ônibus vai demorar para chegar além de auxiliar o motorista montando uma rota que atenda a todos os seus passageiros da melhor maneira possivel, diminuindo o custo da empresa e evitando que algum passageiro seja esquecido. O aluno poderá acompanhar pelo aplicativo no mapa onde o seu ônibus está no momento e poderá compartilhar, ou não, a sua localização com o motorista, a empresa poderá cadastrar um motorista e atribuir uma rota a ele, bem como cadastrar os alunos que fazem parte dessa rota e quando o aluno entra no aplicativo o sistema já identifica que ele está vinculado a uma rota e mostra onde seu ônibus está. 

-------------------------
#Diagrama de classe

![Alt text](https://gitlab.com/Vivannaboa/MinhaConducao/raw/master/Doc/Classe.png)

-------------------------
#Diagrama de caso de uso

![Alt text](https://gitlab.com/Vivannaboa/MinhaConducao/raw/master/Doc/Caso%20de%20uso.png)