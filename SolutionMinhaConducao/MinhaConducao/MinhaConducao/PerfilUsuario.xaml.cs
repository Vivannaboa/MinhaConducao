﻿
using MinhaConducao.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;
using Plugin.Connectivity;
using System.Threading.Tasks;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilUsuario : ContentPage
    {
        bool editando;
        Geocoder geoCoder;

        double lat;
        double lng;

        public PerfilUsuario()
        {
            InitializeComponent();

            dados.IsVisible = false;
            botoes.IsVisible = false;
            toolbar.IsEnabled = false;
            toolbar.Icon = "";
            geoCoder = new Geocoder();
            CarregarDadosAsync();
        }

        private async void CarregarDadosAsync()
        {
            bool conn = await IsConeted();
            if (conn)
            {
                if (App.Passageiro.nome != null && !App.Passageiro.nome.Equals(""))
                {
                    dados.IsVisible = true;
                    botoes.IsVisible = false;
                    conexao.IsVisible = false;
                    lblNome.Text = "Nome:";
                    lblCpf.Text = "CPF:";
                    lblTelefone.Text = "Fone:";
                    lblEndereco.Text = "Endereço:";
                    lblNumero.Text = "Número:";
                    lblBairro.Text = "Bairro:";
                    lblCidade.Text = "Cidade:";

                    entNome.Text = App.Passageiro.nome;
                    entCpf.Text = App.Passageiro.cpf;
                    entTelefone.Text = App.Passageiro.telefone;
                    entEndereco.Text = App.Passageiro.logradouro;
                    entNumero.Text = App.Passageiro.numero;
                    entBairro.Text = App.Passageiro.bairro;
                    entCidade.Text = App.Passageiro.cidade;

                }
                else
                {
                    dados.IsVisible = false;
                    toolbar.IsEnabled = false;
                    botoes.IsVisible = true;
                    botoes_Destino.IsVisible = false;
                    toolbar.Icon = "";
                }

            }
            else
            {
                dados.IsVisible = false;
                botoes.IsVisible = false;
                conexao.IsVisible = true;
                botoes_Destino.IsVisible = false;
                toolbar.Icon = "";
            }
        }

        async Task<bool> IsConeted() => await Api.VerificaConexao();

        async void IncluirDestino(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new DestinoPassageiro());
        }

        async void IncluirDadosPessoais(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PerfilUsuarioDados());
        }

       


        async private void toolbar_ClickedAsync(object sender, EventArgs e)
        {

            if (!editando)
            {
                editando = true;
                toolbar.Icon = "ic_done_white_24dp.png";
                entNome.IsEnabled = true;
                entCpf.IsEnabled = true;
                entTelefone.IsEnabled = true;
                entEndereco.IsEnabled = true;
                entNumero.IsEnabled = true;
                entBairro.IsEnabled = true;
                entCidade.IsEnabled = true;
            }
            else
            {
                editando = false;
                toolbar.Icon = "ic_mode_edit_white_24dp.png";
                entNome.IsEnabled = false;
                entCpf.IsEnabled = false;
                entTelefone.IsEnabled = false;
                entEndereco.IsEnabled = false;
                entNumero.IsEnabled = false;
                entBairro.IsEnabled = false;
                entCidade.IsEnabled = false;

                App.Passageiro.nome = entNome.Text;
                App.Passageiro.cpf = entCpf.Text;
                App.Passageiro.telefone = entTelefone.Text;
                App.Passageiro.logradouro = entEndereco.Text;
                App.Passageiro.numero = entNumero.Text;
                App.Passageiro.bairro = entBairro.Text;
                App.Passageiro.cidade = entCidade.Text;


                //TODO

                var approximateLocations = await geoCoder.GetPositionsForAddressAsync(entEndereco.Text + "," + entNumero.Text + "," + entBairro.Text + "," + entCidade.Text);
                foreach (var position in approximateLocations)
                {
                    lat = position.Latitude;
                    lng = position.Longitude;
                }
                if (lat == 0 && lng == 0)
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 100;
                    var position = await locator.GetPositionAsync();
                    lat = position.Latitude;
                    lng = position.Longitude;
                }

                var content = new FormUrlEncodedContent(new[]
              {
                    new KeyValuePair<string, string>("id", App.Passageiro._id),
                    new KeyValuePair<string, string>("nome", App.Passageiro.nome),
                    new KeyValuePair<string, string>("cpf",App.Passageiro.cpf),
                    new KeyValuePair<string, string>("logradouro",App.Passageiro.logradouro),
                    new KeyValuePair<string, string>("numero", App.Passageiro.numero),
                    new KeyValuePair<string, string>("telefone", App.Passageiro.telefone),
                    new KeyValuePair<string, string>("bairro", App.Passageiro.bairro),
                    new KeyValuePair<string, string>("cidade", App.Passageiro.cidade),
                    new KeyValuePair<string, string>("lat", lat.ToString()),
                    new KeyValuePair<string, string>("lng",lng.ToString())
                 });
                string s = await Api.PutAsync("passageiro", App.token, content);
            }


        }



    }

}

