﻿
using MinhaConducao.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;

namespace MinhaConducao
{
   
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilUsuarioEndereco : ContentPage
    {
        Geocoder geoCoder;
        double lat;
        double lng;

        public PerfilUsuarioEndereco()
        {
            InitializeComponent();
            geoCoder = new Geocoder();
        }
        private async void MenuItem1_ClickedAsync(object sender, EventArgs e)
        {
            try { 
            if (ruaEntry.Text == null || ruaEntry.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "A rua é obrigatória!", cancel: "ok");
                return;
            }
            if (numeroEntry.Text == null || numeroEntry.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "O número é obrigatório!", cancel: "ok");
                return;
            }
            if (bairroEntry.Text == null || bairroEntry.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "O bairro é obrigatório!", cancel: "ok");
                return;
            }
            if (cidadeEntry.Text == null || cidadeEntry.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "A cidade é obrigatória!", cancel: "ok");
                return;
            }
            App.Passageiro.logradouro = ruaEntry.Text;
            App.Passageiro.numero = numeroEntry.Text;
            App.Passageiro.bairro = bairroEntry.Text;
            App.Passageiro.cidade = cidadeEntry.Text;
                var approximateLocations = await geoCoder.GetPositionsForAddressAsync(ruaEntry.Text + "," + numeroEntry.Text + "," + bairroEntry.Text + "," + cidadeEntry.Text);
                foreach (var position in approximateLocations)
                {
                    lat = position.Latitude;
                    lng = position.Longitude;
                }
                if (lat == 0 && lng == 0)
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 100;
                    var position = await locator.GetPositionAsync();
                    lat = position.Latitude;
                    lng = position.Longitude;
                }
                var content = new FormUrlEncodedContent(new[]
               {
                new KeyValuePair<string, string>("nome", App.Passageiro.nome),
                new KeyValuePair<string, string>("cpf",App.Passageiro.cpf),
                new KeyValuePair<string, string>("logradouro",App.Passageiro.logradouro),
                new KeyValuePair<string, string>("numero", App.Passageiro.numero),
                new KeyValuePair<string, string>("telefone", App.Passageiro.telefone),
                new KeyValuePair<string, string>("bairro", App.Passageiro.bairro),
                new KeyValuePair<string, string>("cidade", App.Passageiro.cidade),
                new KeyValuePair<string, string>("lat", lat.ToString().Replace(",",".")),
                new KeyValuePair<string, string>("lng",lng.ToString().Replace(",","."))
                });
            string s = await Api.PostAsync("passageiro",App.token, content);
            if (!Api.status)
            {
                await DisplayAlert(title: "Erro", message:s, cancel: "ok");
                return;
            }
            else
            {
                    App.BuscaPerfilPassageiroAsync();
                    await Navigation.PushAsync(new PerfilUsuario());
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert(title: "Erro", message:ex.Message, cancel: "ok");
            }
        }
    }
}