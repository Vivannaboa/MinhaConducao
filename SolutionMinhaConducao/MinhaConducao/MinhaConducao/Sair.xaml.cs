﻿using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MinhaConducao
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Sair : ContentPage
	{
		public Sair ()
		{
            
            InitializeComponent ();
            sair();
            
        }
        private async void sair()
        {
            App.tipo = "";
            App.IsUserLoggedIn = false;
            App.token = "";
            await App.SaveApplicationProperty("token", "");
            await App.SaveApplicationProperty("tipo", "");
           // Application.Current.MainPage = new LoginPage();
            
        }
	}
}