﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;
using System.Net;
using System.Xml.Linq;
using System;
using System.Collections.Generic;
using MinhaConducao.Entities;
using Newtonsoft.Json;
using Android.Gms.Maps.Model;
using System.Threading.Tasks;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MinhaConducaoView : ContentPage
    {
        List<Passageiro> passageiroList = new List<Passageiro>();
        double lat;
        double lng;
        CustomMap customMap;
        List<Pin> pinos = new List<Pin>();
        public MinhaConducaoView()
        {
            InitializeComponent();
            getPassageiros();
        }

        async void getPassageiros()
        {          

            customMap = new CustomMap
            {
                MapType = MapType.Street,
                WidthRequest = App.ScreenWidth,
                HeightRequest = App.ScreenHeight
            };

            Position posicaoInicial = await getPosicaoMotorista();
            Position positionInstituicao = await getPosicaoInstituicao();
            string pontos = await getPontos();

            Rootobject rootobject = await Api.GetDistanceAsync(posicaoInicial, positionInstituicao,pontos);

            foreach (Route route in rootobject.routes)
            {
                foreach (Leg leg in route.legs)
                {                    
                    foreach (Step step in leg.steps)
                    {
                        List<LatLng> lines = Api.DecodePolyline(step.polyline.points);
                        foreach (var item in lines)
                        {
                            customMap.RouteCoordinates.Add(new Position(item.Latitude, item.Longitude));
                        }
                    }
                }

            }
            foreach (Pin p in pinos)
            {
                customMap.Pins.Add(p);
            }                  

            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-29.638075, -50.845476), Xamarin.Forms.Maps.Distance.FromMiles(1.0)));
            Content = customMap;
        }

        async Task<Position> getPosicaoInstituicao()
        {           
            return new Position(App.Roteiro.latFim, App.Roteiro.lngFim);           
        }

        async Task<Position> getPosicaoMotorista()
        {           
       return new Position(App.Roteiro.latInicio, App.Roteiro.lngInicio);
  
        }
        async Task<string> getPontos()
        {
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = new Position(37.79752, -122.40183)               
            };
            string ret = string.Empty;

            foreach (var item in App.Roteiro.passageiros)
            {
               pinos.Add(new Pin
                {
                    Type = PinType.Place,
                    Position = new Position(item.lat, item.lng),
                    Label = ""
               });
                ret = ret + item.lat.ToString().Replace(",", ".") + "," + item.lng.ToString().Replace(",", ".") + "|";
            }
            return (ret ==""?"":ret.Substring(0,ret.Length-1));
        }
    }
}