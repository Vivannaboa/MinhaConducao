﻿using MinhaConducao.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DestinoPassageiro : ContentPage
    {
        List<String> instituicaiList;
        private bool encontrou;
        private Instituicao instituicao;
        List<Instituicao> listInstituicao;
        Horario hs = new Horario();
        Horario ht = new Horario();
        Horario hq = new Horario();
        Horario hqi = new Horario();
        Horario hse = new Horario();
        Horario hsa = new Horario();
        Horario hd = new Horario();

        public DestinoPassageiro()
        {
            InitializeComponent();
            IniciarAsync();


        }
        private async void IniciarAsync()
        {
            instituicaiList = await GetInstituicaoAsync();
            if (instituicaiList != null)
            {
                autoComplete.DataSource = instituicaiList;
                GetDestino();
            }

        }

        private async void Button_ClickedAsync(object sender, EventArgs e)
        {
           
            if (autoComplete.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "É necessário selecionar uma insttuição!", cancel: "ok");
                return;
            }
            encontrou = false;
            foreach (var item in listInstituicao)
            {
                if (item.Razao_social.Equals(autoComplete.Text))
                {
                    instituicao = item;
                    encontrou = true;
                }
            }
            if (!encontrou)
            {
                await DisplayAlert(title: "Alerta", message: "É necessário selecionar uma insttuição!", cancel: "ok");
                return;
            }
            if (
                SwSegunda.IsToggled ||
                SwTerca.IsToggled ||
                SwQuarta.IsToggled ||
                SwQuinta.IsToggled ||
                SwSexta.IsToggled ||
                SwSabado.IsToggled ||
                SwDomingo.IsToggled
                )
            {
                if (SwSegunda.IsToggled)
                {
                    hs.Dia = "SEGUNDA";
                    hs.Hora = EndTimeSegunda.Time.ToString();
                    hs.Intituicao = instituicao;
                    hs.Pasageiro = App.Passageiro;
                    hs.Ativo = true;
                    await ReristrarDestino(hs);
                }
                else
                {
                    if (hs._id != null && hs._id!=null)
                    {
                        hs.Intituicao = instituicao;
                        hs.Pasageiro = App.Passageiro;
                        hs.Ativo = false;
                        await ReristrarDestino(hs);
                    }
                }
                if (SwTerca.IsToggled)
                {
                    ht.Dia = "TERÇA";
                    ht.Hora = EndTimeTerca.Time.ToString();
                    ht.Intituicao = instituicao;
                    ht.Pasageiro = App.Passageiro;
                    ht.Ativo = true;
                    await ReristrarDestino(ht);
                }
                else
                {
                    if (ht._id != null && ht._id != null)
                    {
                        ht.Intituicao = instituicao;
                        ht.Pasageiro = App.Passageiro;
                        ht.Ativo = false;
                        await ReristrarDestino(ht);
                    }
                }
           
                if (SwQuarta.IsToggled)
                {
                    hq.Dia = "QUARTA";
                    hq.Hora = EndTimeQuarta.Time.ToString();
                    hq.Intituicao = instituicao;
                    hq.Pasageiro = App.Passageiro;
                    hq.Ativo = true;
                    await ReristrarDestino(hq);
                }
                else
                {
                    if (hq._id != null && hq._id != null)
                    {
                        hq.Intituicao = instituicao;
                        hq.Pasageiro = App.Passageiro;
                        hq.Ativo = false;
                        await ReristrarDestino(hq);
                    }
                }
                if (SwQuinta.IsToggled)
                {
                    hqi.Dia = "QUINTA";
                    hqi.Hora = EndTimeQuinta.Time.ToString();
                    hqi.Intituicao = instituicao;
                    hqi.Pasageiro = App.Passageiro;
                    hqi.Ativo = true;
                    await ReristrarDestino(hqi);
                }
                else
                {
                    if (hqi._id != null && hqi._id != null)
                    {
                        hqi.Intituicao = instituicao;
                        hqi.Pasageiro = App.Passageiro;
                        hqi.Ativo = false;
                        await ReristrarDestino(hqi);
                    }
                }
                if (SwSexta.IsToggled)
                {

                    hse.Dia = "SEXTA";
                    hse.Hora = EndTimeSexta.Time.ToString();
                    hse.Intituicao = instituicao;
                    hse.Pasageiro = App.Passageiro;
                    hse.Ativo = true;
                    await ReristrarDestino(hse);
                }
                else
                {
                    if (hse._id != null && hse._id != null)
                    {
                        hse.Intituicao = instituicao;
                        hse.Pasageiro = App.Passageiro;
                        hse.Ativo = false;
                        await ReristrarDestino(hse);
                    }
                }
                if (SwSabado.IsToggled)
                {
                    hsa.Dia = "SABADO";
                    hsa.Hora = EndTimeSabado.Time.ToString();
                    hsa.Intituicao = instituicao;
                    hsa.Pasageiro = App.Passageiro;
                    hsa.Ativo = true;
                    await ReristrarDestino(hsa);
                }
                else
                {
                    if (hsa._id != null && hsa._id != null)
                    {
                        hsa.Intituicao = instituicao;
                        hsa.Pasageiro = App.Passageiro;
                        hsa.Ativo = false;
                        await ReristrarDestino(hsa);
                    }
                }
                if (SwDomingo.IsToggled)
                {
                    hd.Dia = "DOMINGO";
                    hd.Hora = EndTimeDomingo.Time.ToString();
                    hd.Intituicao = instituicao;
                    hd.Pasageiro = App.Passageiro;
                    hd.Ativo = true;
                    await ReristrarDestino(hd);
                }
                else
                {
                    if (hd._id != null && hd._id != null)
                    {
                        hd.Intituicao = instituicao;
                        hd.Pasageiro = App.Passageiro;
                        hd.Ativo = false;
                       await ReristrarDestino(hd);
                    }
                }
            }
            else
            {
                await DisplayAlert(title: "Alerta", message: "É necessário selecionar pelo menos um dia da semana!", cancel: "ok");
                return;
            }
            await Navigation.PushAsync(new PerfilUsuario());
        }
        private async Task<List<string>> GetInstituicaoAsync()
        {
            List<String> instituicaiList = new List<String>();
            listInstituicao = new List<Instituicao>();

            string s = await Api.GetAsync(urlPath: "instituicao", token: App.token);
            if (!Api.status)
            {
                return null;
            }
            listInstituicao = JsonConvert.DeserializeObject<List<Instituicao>>(s);
            foreach (var item in listInstituicao)
            {
                instituicaiList.Add(item.Razao_social);
            }
            return instituicaiList;
        }

        private async Task<bool> ReristrarDestino(Horario item)
        {
            string s = string.Empty;
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("id", item._id),
                new KeyValuePair<string, string>("uid_passageiro", item.Pasageiro._id),
                new KeyValuePair<string, string>("uid_instituicao", item.Intituicao._id),
                new KeyValuePair<string, string>("hora",item.Hora),
                new KeyValuePair<string, string>("dia",item.Dia),
                new KeyValuePair<string, string>("ativo",item.Ativo.ToString().ToLower())
            });
            if (item._id != null)
            {
                s = await Api.PutAsync("horario", App.token, content);
            }
            else
            {
                s = await Api.PostAsync("horario", App.token, content);
            }
            
            if (!Api.status)
            {
                await DisplayAlert(title: "Alerta", message: "Não foi possívelregistrar o distino no servidor!", cancel: "ok");
                return false;
            }
            else
            {
                return true;
            }
        }
        private async void GetDestino()
        {
            try
            {


                var content = new FormUrlEncodedContent(new[]
                  {
                    new KeyValuePair<string, string>("id_passageiro", App.Passageiro._id)
                 });
                string s = await Api.PostAsync("horario/id", App.token, content);
                if (Api.status)
                {

                    List<Horario> horarioList = JsonConvert.DeserializeObject<List<Horario>>(s);
                    if (horarioList.Count > 0)
                    {
                        autoComplete.SelectedValue = "Faccat";

                        foreach (Horario item in horarioList)
                        {
                            switch (item.Dia)
                            {
                                case "SEGUNDA":
                                    if (item.Ativo)
                                    {
                                        SwSegunda.IsToggled = true;
                                        TimeSpan _time = TimeSpan.Parse(item.Hora);
                                        EndTimeSegunda.Time = _time;                                        
                                    }
                                    hs = item;
                                    break;
                                case "TERÇA":
                                    if (item.Ativo)
                                    {
                                        SwTerca.IsToggled = true;
                                        EndTimeTerca.Time = System.TimeSpan.Parse(item.Hora);
                                    }
                                    ht = item;
                                    break;
                                case "QUARTA":
                                    if (item.Ativo)
                                    {
                                        SwQuarta.IsToggled = true;
                                        EndTimeQuarta.Time = System.TimeSpan.Parse(item.Hora);
                                    }
                                    hq = item;
                                    break;
                                case "QUINTA":
                                    if (item.Ativo)
                                    {
                                        SwQuinta.IsToggled = true;
                                        EndTimeQuinta.Time = System.TimeSpan.Parse(item.Hora);
                                    }
                                    hqi = item;
                                    break;
                                case "SEXTA":
                                    if (item.Ativo)
                                    {
                                        SwSexta.IsToggled = true;
                                        EndTimeSexta.Time = System.TimeSpan.Parse(item.Hora);
                                    }
                                    hse = item;
                                    break;
                                case "SABADO":
                                    if (item.Ativo)
                                    {
                                        SwSabado.IsToggled = true;
                                        EndTimeSabado.Time = System.TimeSpan.Parse(item.Hora);
                                    }
                                    hsa = item;
                                    break;
                                case "DOMINGO":
                                    if (item.Ativo)
                                    {
                                        SwDomingo.IsToggled = true;
                                        EndTimeDomingo.Time = System.TimeSpan.Parse(item.Hora);
                                    }
                                    hd = item;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
    }
}