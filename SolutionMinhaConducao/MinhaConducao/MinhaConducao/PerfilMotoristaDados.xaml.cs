﻿
using MinhaConducao.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilMotoristaDados : ContentPage
    {
        Geocoder geoCoder;
        double lat;
        double lng;

        public PerfilMotoristaDados()
        {
            InitializeComponent();
            geoCoder = new Geocoder();
        }
        private async void MenuItem1_ClickedAsync(object sender, EventArgs e)
        {
            try
            {
                if (nomeEntry.Text == null || nomeEntry.Text.Equals(""))
                {
                    await DisplayAlert(title: "Alerta", message: "O nome é obrigatório!", cancel: "ok");
                    return;
                }
                if (cpfEntry.Text == null || cpfEntry.Text.Equals(""))
                {
                    await DisplayAlert(title: "Alerta", message: "O CPF é obrigatório!", cancel: "ok");
                    return;
                }
                if (cnhEntry.Text == null || cnhEntry.Text.Equals(""))
                {
                    await DisplayAlert(title: "Alerta", message: "A CNH é obrigatória!", cancel: "ok");
                    return;
                }
                App.Motorista.Nome = nomeEntry.Text;
                App.Motorista.Cpf = cpfEntry.Text;
                App.Motorista.Cnh = cnhEntry.Text;
                App.Motorista.Telefone = telefoneEntry.Text;

                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;
                var position = await locator.GetPositionAsync();
                lat = position.Latitude;
                lng = position.Longitude;

                var content = new FormUrlEncodedContent(new[]
                  {
                new KeyValuePair<string, string>("nome", App.Motorista.Nome),
                new KeyValuePair<string, string>("cpf",App.Motorista.Cpf),
                new KeyValuePair<string, string>("cnh",App.Motorista.Cnh),
                new KeyValuePair<string, string>("telefone",App.Motorista.Telefone),
                new KeyValuePair<string, string>("lat", lat.ToString().Replace(",",".")),
                new KeyValuePair<string, string>("lng",lng.ToString().Replace(",","."))
                });
                string s = await Api.PostAsync("motorista", App.token, content);
                if (!Api.status)
                {
                    await DisplayAlert(title: "Erro", message: s, cancel: "ok");
                    return;
                }
                else
                {
                    await Navigation.PushAsync(new PerfilMotorista());
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert(title: "Erro", message: ex.Message, cancel: "ok");
            }
        }
    }
}