﻿using Java.Util;
using MinhaConducao.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CriarRoteiro : ContentPage
    {
        List<String> instituicaiList;
        List<Instituicao> listInstituicao;
        private bool encontrou;
        private Instituicao instituicao;
        List<string> dias = new List<string>();
        Roteiro roteiro = new Roteiro();
        public CriarRoteiro()
        {
            InitializeComponent();
            IniciarAsync();
        }

        private async void IniciarAsync()
        {
            instituicaiList = await GetInstituicaoAsync();
            if (instituicaiList != null)
            {
                autoComplete.DataSource = instituicaiList;
            }
            dias.Add("SEGUNDA");
            dias.Add("TERÇA");
            dias.Add("QUARTA");
            dias.Add("QUINTA");
            dias.Add("SEXTA");
            dias.Add("SABADO");
            dias.Add("DOMINGO");
            pikerDias.ItemsSource = dias;
        }
        private async Task<List<string>> GetInstituicaoAsync()
        {
            List<String> instituicaiList = new List<String>();
            listInstituicao = new List<Instituicao>();

            string s = await Api.GetAsync(urlPath: "instituicao", token: App.token);
            if (!Api.status)
            {
                return null;
            }
            listInstituicao = JsonConvert.DeserializeObject<List<Instituicao>>(s);
            foreach (var item in listInstituicao)
            {
                instituicaiList.Add(item.Razao_social);
            }
            return instituicaiList;
        }

        private async void Button_ClickedAsync(object sender, EventArgs e)
        {
            if (autoComplete.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "É necessário selecionar uma insttuição!", cancel: "ok");
                return;
            }
            encontrou = false;
            foreach (var item in listInstituicao)
            {
                if (item.Razao_social.Equals(autoComplete.Text))
                {
                    instituicao = item;
                    encontrou = true;
                }
            }
            if (!encontrou)
            {
                await DisplayAlert(title: "Alerta", message: "É necessário selecionar uma insttuição!", cancel: "ok");
                return;
            }

            roteiro.instituicao = instituicao;
            roteiro.latFim = instituicao.Lat;
            roteiro.lngFim = instituicao.Lng;
            roteiro.latInicio = App.Motorista.Lat;
            roteiro.lngInicio = App.Motorista.Lng;
            roteiro.motorista = App.Motorista;
            roteiro.descricao = pikerDias.SelectedItem.ToString() + new Date().ToString();

            roteiro.horaInicio = hora.Time.ToString();

            string s = await Api.GetAsync("horario", App.token);
            if (!Api.status)
            {
                await DisplayAlert(title: "Alerta", message: s, cancel: "ok");
                //mensagem
                return;
            }

            List<Horario> horarioList = JsonConvert.DeserializeObject<List<Horario>>(s);
            if (horarioList.Count == 0)
            {
                await DisplayAlert(title: "Alerta", message: "Não foi possível encntrar nenhum passageiro!", cancel: "ok");
                //mensagem
                return;
            }
            string p;

            foreach (Horario h in horarioList)
            {
                if (h.Dia.Equals(pikerDias.SelectedItem.ToString()))
                {
                    p = string.Empty;
                    p = await Api.GetAsync("passageiro/id", App.token, h.Uid_passageiro);
                    if (!Api.status)
                    {
                        await DisplayAlert(title: "Alerta", message: p, cancel: "ok");
                        return;
                    }
                    h.Pasageiro = JsonConvert.DeserializeObject<Passageiro>(p);

                    h.Intituicao = instituicao;
                    PassageiroRoteiro pas = new PassageiroRoteiro();
                    pas.id = h.Pasageiro._id;
                    pas.lat = h.Pasageiro.Lat;
                    pas.lng = h.Pasageiro.Lng;

                    roteiro.passageiros.Add(pas);
                }
               

            }

            await CriarRoteiroApi(roteiro);

        }

        private async Task CriarRoteiroApi(Roteiro rot)
        {
            try
            {


                var content = new FormUrlEncodedContent(new[]
              {
                //new KeyValuePair<string, string>("id", rot._id),
                new KeyValuePair<string, string>("descricao", rot.descricao),
                new KeyValuePair<string, string>("horaInicio", rot.horaInicio),
                new KeyValuePair<string, string>("horaFim",rot.horaFim),
                new KeyValuePair<string, string>("latInicio",rot.latInicio.ToString().Replace(",",".")),
                new KeyValuePair<string, string>("latFim",rot.latFim.ToString().Replace(",",".")),
                new KeyValuePair<string, string>("lngInicio",rot.latFim.ToString().Replace(",",".")),
                new KeyValuePair<string, string>("lngFim",rot.latFim.ToString().Replace(",","."))
            });

                string s = await Api.PostAsync("roteiro", App.token, content);
                if (!Api.status)
                {
                    await DisplayAlert(title: "Alerta", message: s, cancel: "ok");
                    //mensagem
                    return;
                }
                Roteiro retorno = JsonConvert.DeserializeObject<Roteiro>(s);

                App.Roteiro = rot;
                await Navigation.PushAsync(new MinhaConducaoView());
                //foreach (var item in rot.passageiros)
                //{
                //    var contentChild = new FormUrlEncodedContent(new[]
                //        {
                //            new KeyValuePair<string, string>("id",retorno._id),
                //            new KeyValuePair<string, string>("id_passageiro", item.id)


                //   });
                //    string w = await Api.PostAsync("roteiro/addPassageiro", App.token, contentChild);
                //    if (!Api.status)
                //    {
                //        await DisplayAlert(title: "Alerta", message: w, cancel: "ok");
                //        //mensagem
                //        return;
                //    }
                //}
            }
            catch (Exception ex)
            {
                await DisplayAlert(title: "Alerta", message: ex.Message, cancel: "ok");
            }

        }

    }
}
