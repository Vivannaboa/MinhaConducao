﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MinhaConducao.Entities;
using System.Net.Http;
using Newtonsoft.Json;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();

        }
        async void OnSignUpButtonClicked(object sender, EventArgs e)
        {

            var rootPage = Navigation.NavigationStack.FirstOrDefault();
            if (rootPage != null)
            {
                await Navigation.PushAsync(new SignUpPage());
            }
            else
            {
                   new NavigationPage(new LoginPage());
            }
            
        }
        async void OnLoginButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Usuario _usuario2 = Usuario.Instance;
                _usuario2.email = usernameEntry.Text;
                _usuario2.senha = passwordEntry.Text;

                var isValid = AreCredentialsCorrectAsync(_usuario2);
                if (await isValid)
                {
                    App.IsUserLoggedIn = true;
                    if (Device.OS == TargetPlatform.iOS)
                    {
                        await Navigation.PopToRootAsync();
                    }
                    Application.Current.MainPage = new MasterDetailPagePrincipal();
                }
                else
                {
                    messageLabel.Text = "Login failed";
                    passwordEntry.Text = string.Empty;
                }
            }
            catch (Exception err)
            {
                err.Message.ToString();
            }
        }
        async Task<bool> AreCredentialsCorrectAsync(Usuario user)
        {
            var content = new FormUrlEncodedContent(new[]
                {
                new KeyValuePair<string, string>("email", user.email),
                new KeyValuePair<string, string>("senha", user.senha)
                });

            String s = await Api.PostAsync("usuario/login", null, content);

            if (!Api.status)
            {
                messageLabel.Text = s;
                return false;
            }
            else
            {
                Usuario _usuario = Usuario.Instance;
                _usuario = JsonConvert.DeserializeObject<Usuario>(s);
                await App.SaveApplicationProperty("token", _usuario.token);
                await App.SaveApplicationProperty("tipo", _usuario.tipo);
                App.token = _usuario.token;
                App.tipo= _usuario.tipo;
                if (_usuario.tipo.Equals("MOTORISTA"))
                {
                    App.BuscaPerfilMotoristaAsync();                   
                }
                else
                {
                    App.BuscaPerfilPassageiroAsync();
                }
               
                return true;
            }


        }


    }
}