﻿using MinhaConducao.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MinhaConducao
{
    public partial class App : Application
    {
        public static double ScreenHeight;
        public static double ScreenWidth;
        public static bool IsUserLoggedIn { get; set; }
        
        internal static Passageiro Passageiro { get => passageiro; set => passageiro = value; }
        internal static Roteiro Roteiro { get => roteiro; set => roteiro = value; }
        internal static Motorista Motorista { get => motorista; set => motorista = value; }

        private static Motorista motorista = new Motorista();

        private static Passageiro passageiro = new Passageiro();
        private static Roteiro roteiro = new Roteiro();
        public static string token;
        public static string tipo;
        
        public App()
        {
            InitializeComponent();
            token = LoadApplicationProperty<string>("token");
            tipo = LoadApplicationProperty<string>("tipo");
            if (token == null || tipo == null || token.Equals("") || tipo.Equals(""))
            {
                MainPage = new NavigationPage(new LoginPage());
            }
            else
            {
                GetUser(token);
                if (tipo.Equals("MOTORISTA"))
                {
                    BuscaPerfilMotoristaAsync();
                }
                else
                {
                    BuscaPerfilPassageiroAsync();
                }                
                 MainPage = new MasterDetailPagePrincipal();
                //Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                //{
                //    Console.Write("Pasoou");
                //    return true;
                //});
            }
        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static async Task SaveApplicationProperty<T>(string key, T value)
        {
            if (App.Current.Properties.ContainsKey(key))
                App.Current.Properties[key] = value;
            else
                App.Current.Properties.Add(key, value);
            await App.Current.SavePropertiesAsync();
        }

        public T LoadApplicationProperty<T>(string key)
        {
            if (App.Current.Properties.ContainsKey(key))
            {
                return (T)Xamarin.Forms.Application.Current.Properties[key];
            }
            else
            {
                return default(T);
            }
        }

        async Task GetUser(string token)
        {
            String s = await Api.GetAsync("usuario/listar", token);
            Usuario usuario = Usuario.Instance;
            if (Api.status)
            {
                usuario = JsonConvert.DeserializeObject<Usuario>(s);
                Usuario.Instance.email = usuario.email;
                Usuario.Instance.tipo = usuario.tipo;
                Usuario.Instance.token = usuario.token;
                Usuario.Instance._id = usuario._id;
                Usuario.Instance.senha = usuario.senha;
            }
        }

        public async static void BuscaPerfilPassageiroAsync()
        {
            List<Passageiro> list = new List<Passageiro>();
            try
            {
                String s = await Api.GetAsync("passageiro/token", token);
                if (Api.status)
                {
                    list = JsonConvert.DeserializeObject<List<Passageiro>>(s);
                    if (list.Count > 0)
                    {
                        passageiro = list[0];
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

        }

        public async static void BuscaPerfilMotoristaAsync()
        {
            List<Motorista> listMotorist = new List<Motorista>();
            try
            {
                String s = await Api.GetAsync("motorista/token", token);
                if (Api.status)
                {
                    listMotorist = JsonConvert.DeserializeObject<List<Motorista>>(s);
                    if (listMotorist.Count > 0)
                    {
                        motorista = listMotorist[0];
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

        }

    }
}
