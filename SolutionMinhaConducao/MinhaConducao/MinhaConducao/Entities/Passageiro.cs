﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinhaConducao.Entities
{
    class Passageiro
    {
        public string _id { get; set; }
        public DateTime updatedAt { get; set; }
        public DateTime createdAt { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string cidade { get; set; }
        public string bairro { get; set; }
        public string telefone { get; set; }
        public string numero { get; set; }
        public string logradouro { get; set; }
        public string cpf { get; set; }
        public string nome { get; set; }
        public int __v { get; set; }
    }
}
