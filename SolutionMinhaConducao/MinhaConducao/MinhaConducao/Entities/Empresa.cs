﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinhaConducao.Entities
{
    class Empresa
    {
        public int __v { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Token_organizacao { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public int Numero { get; set; }
        public string Logradouro { get; set; }
        public string Cnpj { get; set; }
        public string Razao_social { get; set; }
        public string _id { get; set; }
    }
}

