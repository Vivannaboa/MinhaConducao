﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinhaConducao.Entities
{
    class Motorista
    {
        public int __v { get; set; }
        public string Telefone { get; set; }
        public string Cnh { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string _id { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }

        public double LatAtual { get; set; }
        public double LngAtual { get; set; }
    }
}
