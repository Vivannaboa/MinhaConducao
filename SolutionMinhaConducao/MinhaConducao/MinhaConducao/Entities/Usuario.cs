﻿
namespace MinhaConducao.Entities
{

    public class Usuario
    {
        private static Usuario instance;

        private Usuario() { }

        public static Usuario Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Usuario();
                }
                return instance;
            }
        }
        public int __v { get; set; }
        public string token { get; set; }
        public string senha { get; set; }
        public string email { get; set; }
        public string tipo { get; set; }
        public string _id { get; set; }
    }
    
    
}
