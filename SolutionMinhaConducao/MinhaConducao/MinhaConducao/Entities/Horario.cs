﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinhaConducao.Entities
{
    class Horario
    {
        public int __v { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Uid_passageiro { get; set; }
        public string Uid_instituicao { get; set; }
        public string Hora { get; set; }
        public string Dia { get; set; }
        public string _id { get; set; }
        public bool Excluido { get; set; }
        public bool Ativo { get; set; }
        public Passageiro Pasageiro { get; set; }
        public Instituicao Intituicao { get; set; }


    }
}

