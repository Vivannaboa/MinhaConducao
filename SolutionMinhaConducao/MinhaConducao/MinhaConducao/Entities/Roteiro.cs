﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinhaConducao.Entities
{
    class Roteiro
    {
        public Roteiro()
        {
            passageiros = new List<PassageiroRoteiro>();
        }
        
        public string _id { get; set; }
        public DateTime updatedAt { get; set; }
        public DateTime createdAt { get; set; }
        public double lngFim { get; set; }
        public double lngInicio { get; set; }
        public double latFim { get; set; }
        public double latInicio { get; set; }
        public string horaFim { get; set; }
        public string horaInicio { get; set; }
        public string descricao { get; set; }
        public int __v { get; set; }
        public Instituicao instituicao { get; set; }
        public Motorista motorista { get; set; }
        public virtual List<PassageiroRoteiro> passageiros { get; set; }
    }
}

public class PassageiroRoteiro
{
    public string id { get; set; }
    public double lat { get; set; }
    public double lng { get; set; }
    public string _id { get; set; }
}
