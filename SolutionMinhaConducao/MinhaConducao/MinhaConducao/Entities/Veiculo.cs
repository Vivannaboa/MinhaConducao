﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinhaConducao.Entities
{
    class Veiculo
    {
        public int __v { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public int Capacidade { get; set; }
        public string Modelo { get; set; }
        public string Placa { get; set; }
        public string _id { get; set; }
        public bool Excluido { get; set; }
    }
}
