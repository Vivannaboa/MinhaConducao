﻿using System.Threading.Tasks;
using System.Net.Http;
using Plugin.Connectivity;
using Newtonsoft.Json;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using System;
using Android.Gms.Maps.Model;

namespace MinhaConducao.Entities
{
    public static class Api
    {
        static string ip = "192.168.1.115";
        static int porta = 5000;
        public static string UrlApi = "http://" + ip + ":" + porta.ToString() + "/";
        public static bool status = false;

        public static async Task<string> GetAsync(string urlPath, string token, string uid = "")
        {
            HttpClient client = new HttpClient();
            string retorno;
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("aplication/json"));
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(token);
            client.DefaultRequestHeaders.Add("uid", uid);
            HttpResponseMessage response = await client.GetAsync(UrlApi + urlPath);
            status = response.IsSuccessStatusCode;
            if (status)
            {
                retorno = await response.Content.ReadAsStringAsync();
            }
            else
            {
                retorno = response.Content.ReadAsStringAsync().Result;
            }

            return retorno;
        }

        public static async Task<string> PostAsync(string urlPath, string token, FormUrlEncodedContent body)
        {
            HttpClient client = new HttpClient();
            string retorno;

            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("aplication/json"));
            if (token != null)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(token);
            }
            HttpResponseMessage response = await client.PostAsync(UrlApi + urlPath, body);
            status = response.IsSuccessStatusCode;
            if (status)
            {
                retorno = await response.Content.ReadAsStringAsync();
            }
            else
            {
                retorno = response.Content.ReadAsStringAsync().Result;
            }
            return retorno;
        }

        public static async Task<string> PutAsync(string urlPath, string token, FormUrlEncodedContent body)
        {
            HttpClient client = new HttpClient();
            string retorno;

            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("aplication/json"));
            if (token != null)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(token);
            }
            //acessando um servidor qualquer - await indica o início do processo assincrono
            HttpResponseMessage response = await client.PutAsync(UrlApi + urlPath, body);
            status = response.IsSuccessStatusCode;
            if (status)
            {
                retorno = await response.Content.ReadAsStringAsync();
            }
            else
            {
                retorno = response.Content.ReadAsStringAsync().Result;
            }
            return retorno;
        }

        public static async Task<string> DeleteAsync(string urlPath, string token, FormUrlEncodedContent body)
        {
            HttpClient client = new HttpClient();
            string retorno;

            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("aplication/json"));
            if (token != null)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(token);
            }
            //acessando um servidor qualquer - await indica o início do processo assincrono
            HttpResponseMessage response = await client.DeleteAsync(UrlApi + urlPath + "");
            status = response.IsSuccessStatusCode;
            if (status)
            {
                retorno = await response.Content.ReadAsStringAsync();
            }
            else
            {
                retorno = response.Content.ReadAsStringAsync().Result;
            }
            return retorno;
        }

        public static async Task<bool> VerificaConexao()
        {
            bool ret = false;
            if (CrossConnectivity.Current.IsConnected)
            {
                var reachable = await CrossConnectivity.Current.IsRemoteReachable(ip, porta, 5000);


                if (reachable)
                {
                    ret = true;
                }
            }
            return ret;

        }

        public static async Task<Rootobject>  GetDistanceAsync(Position origin, Position destination,string pontos)
        {
            string apiUrl = "https://maps.googleapis.com/maps/api/directions/json?origin={0},{1}&waypoints={4}&destination={2},{3}&sensor=false&key=AIzaSyCtWLxwKcalTtT5O4iV8wu4oMaZNXHyceE";

            apiUrl = string.Format(apiUrl,
                origin.Latitude.ToString().Replace(",", "."), origin.Longitude.ToString().Replace(",", "."),
                destination.Latitude.ToString().Replace(",", "."), destination.Longitude.ToString().Replace(",", "."),
                pontos
                );
            HttpClient client = new HttpClient();
            string retorno;

            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("aplication/json"));
            HttpResponseMessage response = await client.GetAsync(apiUrl);

            retorno = await response.Content.ReadAsStringAsync();

            Rootobject rootobject = JsonConvert.DeserializeObject<Rootobject>(retorno);

            return rootobject;

        }


        public static List<LatLng> DecodePolyline(string encodedPoints)
        {
            if (string.IsNullOrWhiteSpace(encodedPoints))
            {
                return null;
            }

            int index = 0;
            var polylineChars = encodedPoints.ToCharArray();
            var poly = new List<LatLng>();
            int currentLat = 0;
            int currentLng = 0;
            int next5Bits;

            while (index < polylineChars.Length)
            {
                // calculate next latitude
                int sum = 0;
                int shifter = 0;

                do
                {
                    next5Bits = polylineChars[index++] - 63;
                    sum |= (next5Bits & 31) << shifter;
                    shifter += 5;
                }
                while (next5Bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length)
                {
                    break;
                }

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                // calculate next longitude
                sum = 0;
                shifter = 0;

                do
                {
                    next5Bits = polylineChars[index++] - 63;
                    sum |= (next5Bits & 31) << shifter;
                    shifter += 5;
                }
                while (next5Bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length && next5Bits >= 32)
                {
                    break;
                }

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                var mLatLng = new LatLng(Convert.ToDouble(currentLat) / 100000.0, Convert.ToDouble(currentLng) / 100000.0);
                poly.Add(mLatLng);
            }

            return poly;
        }

    }

}

public class Rootobject
{
    public Geocoded_Waypoints[] geocoded_waypoints { get; set; }
    public Route[] routes { get; set; }
    public string status { get; set; }
}

public class Geocoded_Waypoints
{
    public string geocoder_status { get; set; }
    public string place_id { get; set; }
    public string[] types { get; set; }
}

public class Route
{
    public Bounds bounds { get; set; }
    public string copyrights { get; set; }
    public Leg[] legs { get; set; }
    public Overview_Polyline overview_polyline { get; set; }
    public string summary { get; set; }
    public object[] warnings { get; set; }
    public object[] waypoint_order { get; set; }
}

public class Bounds
{
    public Northeast northeast { get; set; }
    public Southwest southwest { get; set; }
}

public class Northeast
{
    public float lat { get; set; }
    public float lng { get; set; }
}

public class Southwest
{
    public float lat { get; set; }
    public float lng { get; set; }
}

public class Overview_Polyline
{
    public string points { get; set; }
}

public class Leg
{
    public Distance distance { get; set; }
    public Duration duration { get; set; }
    public string end_address { get; set; }
    public End_Location end_location { get; set; }
    public string start_address { get; set; }
    public Start_Location start_location { get; set; }
    public Step[] steps { get; set; }
    public object[] traffic_speed_entry { get; set; }
    public object[] via_waypoint { get; set; }
}

public class Distance
{
    public string text { get; set; }
    public int value { get; set; }
}

public class Duration
{
    public string text { get; set; }
    public int value { get; set; }
}

public class End_Location
{
    public float lat { get; set; }
    public float lng { get; set; }
}

public class Start_Location
{
    public float lat { get; set; }
    public float lng { get; set; }
}

public class Step
{
    public Distance1 distance { get; set; }
    public Duration1 duration { get; set; }
    public End_Location1 end_location { get; set; }
    public string html_instructions { get; set; }
    public Polyline polyline { get; set; }
    public Start_Location1 start_location { get; set; }
    public string travel_mode { get; set; }
    public string maneuver { get; set; }
}

public class Distance1
{
    public string text { get; set; }
    public int value { get; set; }
}

public class Duration1
{
    public string text { get; set; }
    public int value { get; set; }
}

public class End_Location1
{
    public float lat { get; set; }
    public float lng { get; set; }
}

public class Polyline
{
    public string points { get; set; }
}

public class Start_Location1
{
    public float lat { get; set; }
    public float lng { get; set; }
}

