﻿using MinhaConducao.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MinhaConducao
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PerfilUsuarioDados : ContentPage
	{
		public PerfilUsuarioDados ()
		{
			InitializeComponent ();
		}

        private async void MenuItem1_ClickedAsync(object sender, EventArgs e)
        {
            if (nomeEntry.Text == null || nomeEntry.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "O nome é obrigatório!", cancel: "ok");
                return;
            }
            if (cpfEntry.Text == null || cpfEntry.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "O CPF é obrigatório!", cancel: "ok");
                return;
            }
            if (telefoneEntry.Text == null || telefoneEntry.Text.Equals(""))
            {
                await DisplayAlert(title: "Alerta", message: "O telefone é obrigatório!", cancel: "ok");
                return;
            }
            App.Passageiro.nome = nomeEntry.Text;
            App.Passageiro.cpf = cpfEntry.Text;
            App.Passageiro.telefone = telefoneEntry.Text;            
            await Navigation.PushAsync(new PerfilUsuarioEndereco());
        }

        private void MenuItem1_Clicked(object sender, EventArgs e)
        {

        }
    }
}