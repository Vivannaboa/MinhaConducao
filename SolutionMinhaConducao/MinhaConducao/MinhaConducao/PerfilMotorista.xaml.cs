﻿using MinhaConducao.Entities;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilMotorista : ContentPage
    {
        bool editando;

        public PerfilMotorista()
        {

            InitializeComponent();
            dadosMotorista.IsVisible = false;
            botoes.IsVisible = false;
            conexao.IsVisible = true;
            toolbar.Icon = "";
            CarregarDadosAsync();
        }

        async void IncluirDadosMotorista(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PerfilMotoristaDados());
        }

        async void confirmarCadastro(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        async Task<bool> IsConeted() => await Api.VerificaConexao();

        private async void CarregarDadosAsync()
        {
            bool conn = await IsConeted();
            if (conn)
            {
                if (App.Motorista.Nome != null && !App.Motorista.Nome.Equals(""))
                {

                    dadosMotorista.IsVisible = true;
                    botoes.IsVisible = false;
                    toolbar.Icon = "ic_mode_edit_white_24dp.png";
                    toolbar.IsEnabled = true;
                    conexao.IsVisible = false;
                    lblNome.Text = "Nome:";
                    lblCpf.Text = "CPF:";
                    lblCnh.Text = "CNH:";
                    lblTelefone.Text = "Telefone:";

                    entNome.Text = App.Motorista.Nome;
                    entCpf.Text = App.Motorista.Cpf;
                    entCnh.Text = App.Motorista.Cnh;
                    entTelefone.Text = App.Motorista.Telefone;
                }
                else
                {
                    dadosMotorista.IsVisible = false;
                    toolbar.IsEnabled = false;
                    toolbar.Icon = "";
                    conexao.IsVisible = false;
                    botoes.IsVisible = true;
                }
            }
            else
            {
                dadosMotorista.IsVisible = false;
                botoes.IsVisible = false;
                conexao.IsVisible = true;
                toolbar.Icon = "";
            }
        }


        async private void toolbar_ClickedAsync(object sender, EventArgs e)
        {

            if (!editando)
            {
                editando = true;
                toolbar.Icon = "ic_done_white_24dp.png";
                entNome.IsEnabled = true;
                entCpf.IsEnabled = true;
                entCnh.IsEnabled = true;
                entTelefone.IsEnabled = true;
            }
            else
            {
                editando = false;
                toolbar.Icon = "ic_mode_edit_white_24dp.png";
                entNome.IsEnabled = false;
                entCpf.IsEnabled = false;
                entCnh.IsEnabled = false;
                entTelefone.IsEnabled = false;


                App.Motorista.Nome = entNome.Text;
                App.Motorista.Cpf = entCpf.Text;
                App.Motorista.Cnh = entCnh.Text;
                App.Motorista.Telefone = entTelefone.Text;

                var content = new FormUrlEncodedContent(new[]
                  {
                new KeyValuePair<string, string>("id", App.Motorista._id),
                new KeyValuePair<string, string>("nome", App.Motorista.Nome),
                new KeyValuePair<string, string>("cpf",App.Motorista.Cpf),
                new KeyValuePair<string, string>("cnh",App.Motorista.Cnh),
                new KeyValuePair<string, string>("telefone",App.Motorista.Telefone)
                });
                string s = await Api.PutAsync("motorista", App.token, content);
                if (!Api.status)
                {
                    await DisplayAlert(title: "Erro", message: s, cancel: "ok");
                    return;
                }


            }
        }
    }
}