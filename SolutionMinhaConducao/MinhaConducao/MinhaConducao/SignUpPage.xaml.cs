﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MinhaConducao.Entities;
using System.Net.Http;
using Newtonsoft.Json;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage : ContentPage
    {
        public SignUpPage()
        {
            InitializeComponent();
        }
      
        async void OnSignUpButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Usuario _usuario = Usuario.Instance;
                _usuario.email = emailEntry.Text;
                _usuario.senha = passwordEntry.Text;
                _usuario.tipo = selectTipo();
               

                // Sign up logic goes here
                var signUpSucceeded = AreDetailsValid(_usuario);      

                var content = new FormUrlEncodedContent(new[]
                {
                new KeyValuePair<string, string>("email", _usuario.email),
                new KeyValuePair<string, string>("senha", _usuario.senha),
                new KeyValuePair<string, string>("tipo", _usuario.tipo)
                });

                String s = await Api.PostAsync("usuario/cadastrar", null, content);

                if (!Api.status)
                {
                    messageLabel.Text = s;
                    return;
                }
                                
                if (signUpSucceeded)
                {
                    Usuario _usuario2 = Usuario.Instance;
                    _usuario2 = JsonConvert.DeserializeObject<Usuario>(s);
                    await App.SaveApplicationProperty("token", _usuario2.token);
                    await App.SaveApplicationProperty("tipo", _usuario2.tipo);
                    App.token = _usuario2.token;
                    App.tipo = _usuario2.tipo;
                    if (_usuario2.tipo.Equals("MOTORISTA"))
                    {
                        App.BuscaPerfilMotoristaAsync();
                    }
                    else
                    {
                        App.BuscaPerfilPassageiroAsync();
                    }
                    var rootPage = Navigation.NavigationStack.FirstOrDefault();
                    if (rootPage != null)
                    {                        
                        App.IsUserLoggedIn = true;
                        App.tipo = _usuario2.tipo;
                        if (Device.OS == TargetPlatform.iOS)
                        {
                            await Navigation.PopToRootAsync();
                        }
                        Application.Current.MainPage = new MasterDetailPagePrincipal();

                    }

                }
                else
                {
                    messageLabel.Text = "Erro ao criar conta.";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                messageLabel.Text = "Erro ao cadastrar usuário." + ex.ToString();
                return;
            }

        }

        private string selectTipo()
        {
            if (swMotorista.IsToggled)
            {
                return "MOTORISTA";
            }
            else
            {
                return "Usuario";
            }
        }

        bool AreDetailsValid(Usuario user)
        {
            return (!string.IsNullOrWhiteSpace(user.email) && !string.IsNullOrWhiteSpace(user.senha) && !string.IsNullOrWhiteSpace(user.email) && user.email.Contains("@"));
        }

        void SwitchToolge(object sender, EventArgs e)
        {
            if (swMotorista.IsToggled)
            {
                motorista.TextColor = Color.Black;
                usuario.TextColor = Color.Silver;
            }
            else
            {
                motorista.TextColor = Color.Silver;
                usuario.TextColor = Color.Black;
            }
        }
    }
}