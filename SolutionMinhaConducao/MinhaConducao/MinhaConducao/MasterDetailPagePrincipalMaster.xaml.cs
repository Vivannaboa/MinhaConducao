﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MinhaConducao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailPagePrincipalMaster : ContentPage
    {
        public ListView ListView;

        public MasterDetailPagePrincipalMaster()
        {
            InitializeComponent();

            BindingContext = new MasterDetailPagePrincipalMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MasterDetailPagePrincipalMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MasterDetailPagePrincipalMenuItem> MenuItems { get; set; }
            
            public MasterDetailPagePrincipalMasterViewModel()
            {
                MenuItems = new ObservableCollection<MasterDetailPagePrincipalMenuItem>(new[]
                {

                    new MasterDetailPagePrincipalMenuItem { Id = 0, Title = "Perfil", TargetType= (App.tipo.Equals("MOTORISTA"))? typeof(PerfilMotorista):typeof(PerfilUsuario) },
                    new MasterDetailPagePrincipalMenuItem { Id = 1, Title = (App.tipo.Equals("MOTORISTA")? "Criar Roteiro":"Ver minha condução"),TargetType = (App.tipo.Equals("MOTORISTA")? typeof(CriarRoteiro) :typeof(MinhaConducaoView)) },
                    //new MasterDetailPagePrincipalMenuItem { Id = 2, Title = "Page 3" },
                    //new MasterDetailPagePrincipalMenuItem { Id = 3, Title = "Page 4" },
                    new MasterDetailPagePrincipalMenuItem { Id = 4, Title = "Sair", TargetType=typeof(Sair) },
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}