﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MinhaConducao
{
    class Gif : WebView
    {
        public string GifSource
        {
            set
            {
                string imgSource = DependencyService.Get<IGif>().Get() + value;
                var html = new HtmlWebViewSource();
                html.Html = String.Format(@"<html><body><img src='{0}'  style='width: 100px; height: 100px;' /></body></html>", imgSource);
                SetValue(SourceProperty, html);
            }
        }
    }
}
